﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace WpfApp_lesson7.Models
{
    public class Item : INotifyPropertyChanged
    {
        private string _name;


        public int Id { get; set; }
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                UpdatePropety("Name");
            }
        }
        public string Description { get; set; }
        public int Price { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void UpdatePropety([CallerMemberName]string propetyName="")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propetyName));
        }
    }
}